# remote server connect

Add your linux servers IP to list-of-hosts file and run ansible

Ansible uses ssh to connect and apply changes

So once you can ssh to an IP from your local machine then so can ansible from your local machine

Go to the folder which has these below files and run:

`ansible-playbook --inventory-file list-of-hosts remote-server-playbook.yml -vv`

